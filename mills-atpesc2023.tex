\documentclass[aspectratio=169,9pt]{beamer}

\include{include/header}
\include{include/headerLocal}
\usepackage{JedMacros-modified}
\usepackage{comment}
\newcommand{\M}{\mathcal{M}}
\newcommand{\pc}{\ensuremath{-}}
\newcommand{\lp}{\ensuremath{\pc_{L}}}
\newcommand{\rp}{\ensuremath{\pc_{R}}}
\newcommand{\lin}{\ensuremath{\backslash}}
\newcommand{\solvername}[1]{\ensuremath{\begingroup\text{#1}\endgroup}}
\newcommand{\krylov}{\solvername{K}}
\newcommand{\NEWT}{\mathcal{N}}

\lstloadlanguages{C++}
\lstset{%
  language=C++,
  basicstyle=\footnotesize\ttfamily,
  commentstyle=\itshape\color{darkgreen},
  keywordstyle=\bfseries\color{darkblue},
  stringstyle=\color{darkred},
  showspaces=false,
  showtabs=false,
  columns=fixed,
%  backgroundcolor=\color{lightgrey},
  numbers=none,
  frame=single,
  numberstyle=\tiny,
  breaklines=true,
  breakatwhitespace=true, % Need this so we don't break command lines options
  showstringspaces=false,
  xleftmargin=0.1cm,
  xrightmargin=3em
}%

\lstset{emphstyle=\color{red}}

% Citations (ornery)
\usepackage[backend=bibtex,maxnames=100,doi=false]{biblatex} %from macports texlive-bibtex-extra
\addbibresource{refs.bib}
\renewbibmacro{in:}{} % remove "In:" for journal
%\renewcommand{\footnotesize}{\tiny}

\date{August 8, 2023}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{document}
\setitemize{topsep=2pt,partopsep=0pt,parsep=0pt,leftmargin=*,label=\textbullet}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% ... Title Page
\setbeamertemplate{footline}{}
{

\usebackgroundtemplate{\parbox[b][\paperheight][b]{\paperwidth}{\includegraphics[page=1,width=\paperwidth]{ATPESC_template}}}
\begin{frame}[t]{}
\begin{textblock}{80}(4.4,15)
{ \huge \bf
Introduction to Nonlinear Solvers Using PETSc/TAO }
\end{textblock}

\begin{textblock}{80}(4.4,30)
Presented to\\
{\bf ATPESC 2023 Participants}
\end{textblock}

\begin{textblock}{80}(4.4,48)
Richard Tran Mills \\
Argonne National Laboratory \\
August 8, 2023

\vspace{0.01in}
\hspace{-0.12in}
\includegraphics[width=0.30\linewidth]{figures/PETSc-TAO_RGB.png}

\end{textblock}

%\hspace{-.1in}

\end{frame}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\setbeamertemplate{footline}[frame number]{}
\setbeamertemplate{footline}[text line]{%
  \parbox{\linewidth}{\vspace{-3em} \insertpagenumber ~ ATPESC 2023, July 30 -- August 11, 2023}}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}{Iterative Solvers for Nonlinear Systems}
% Note that, unlike linear systems, one cannot in general solve nonlinear systems in a finite number of steps:
%  Iterative methods required!
% Introduce basic iteration (Richardson), extensions to nonlinear Krylov-like methods.

Systems of nonlinear equations 

\begin{equation}
F(x) = b \quad \mathrm{where} \quad F : \R^N \to \R^N
\end{equation}

arise in countless settings in computational science.\\
Direct methods for general nonlinear systems do not exist.
Iterative methods are required!

%%%
\begin{block}{Nonlinear Richardson (simple) iteration:}

\begin{equation}
x_{k+1} = x_k + \lambda (b - F(x_k))
\end{equation}

This has linear convergence at best: $\|e_{k+1}\| \leq C \|e_k\|$
\end{block}
%%%

%%%
\begin{block}{Nonlinear Krylov methods}
\textcolor{red}{Nonlinear CG} - Mimic CG to force each new search direction to be orthogonal to previous directions.

\textcolor{red}{Nonlinear GMRES (Anderson mixing)} - minimize $\|F(x_{k+1} - b)\|$ by using $x_{k+1}$ as a linear combination of previous
solutions and solving a linear least squares problem.

These have superlinear convergence at best: $\|e_{k+1}\| \leq C \|e_k\|^{\alpha \geq 1}$
\end{block}

\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\input{slides/SNES/SNESNewton.tex}
% Need to talk about globalization (line search is the instance we'll discuss).
% From http://homepages.rpi.edu/home/37/hickej2/yesterday/public_html/papers/AIAA-2009-4139.pdf:
% "globalization seeks to move arbitrary initial iterates into the basin of attraction of Newton's method"
% Also need a slide on inexact Newton and Newton-Krylov.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}{\Huge High-Performance Nonlinear Solvers Are Available Via}

\begin{minipage}[t]{0.3 \textwidth}
\includegraphics[width=\linewidth]{figures/PETSc-TAO_RGB.png}
\end{minipage} \\
%\begin{minipage}[t]{0.66 \textwidth}
\href{https://www.mcs.anl.gov/petsc/}{\blue{PETSc: Portable, Extensible Toolkit for Scientific Computation}} \\
%\end{minipage}
~ \\

\begin{minipage}[t]{0.1 \textwidth}
\includegraphics[width=\linewidth]{figures/sundials_logo_higher_contrast.png}
\end{minipage}
\begin{minipage}[t]{0.88 \textwidth}
\vspace{-.32in}
\href{https://computing.llnl.gov/projects/sundials}{\blue{SUNDIALS: SUite of Nonlinear and DIfferential/ALgebraic Equation Solvers}} \\
\end{minipage} \\
~ \\

\begin{minipage}[t]{0.1 \textwidth}
\includegraphics[width= \linewidth]{figures/trilinos.jpg}
\end{minipage}
\begin{minipage}[t]{0.88 \textwidth}
\vspace{-.27in}
Trilinos: \href{https://trilinos.github.io/nox_and_loca.html}{\blue{NOX (Nonlinear Object-Oriented Solutions) and \\ LOCA (Library of Continuation Algorithms)}} packages. \\
\end{minipage} \\
~ \\

{\Large \bf This presentation focuses on PETSc.}

\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\usebackgroundtemplate{\parbox[b][\paperheight][b]{\paperwidth}{\includegraphics[page=1,width=\paperwidth]{powerpoint/petsc-overview}}}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%% Reset things back to the normal template we are using,
%%% after using some background template trickery to include PDF versions of PowerPoint slides.
\usebackgroundtemplate{\parbox[b][\paperheight][b]{\paperwidth}{\includegraphics[page=2,width=\paperwidth]{ATPESC_template}}}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% PETSc overview diagram from Patrick's PETSC+GPUs tutorial at 2022 CIG Developer's Workshop
\begin{frame}[fragile]
\frametitle{PETSc - the Portable, Extensible Toolkit for Scientific computation}
  \begin{center}
    \includegraphics[width=0.9\textwidth]{figures/petsc_overview.png}
  \end{center}
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}{\Huge PETSc is a platform for experimentation!}

Optimal solvers must consider the interplay among physics, algorithms, and architectures.\\
This need motivates several elements of the PETSc design philosophy:

\begin{block}{Algebraic solvers must be}
\begin{itemize}
\item Composable: Separately developed solvers should be easy to combine, by
non-experts, to form a more powerful solver.
\item Hierarchical: Outer solvers may iterate over all variables for a global
problem, while inner solvers handle smaller subsets of physics, smaller
physical subdomains, or coarser meshes.
\item Nested: Outer solvers call nested inner solvers
\item Extensible: Easily customized or extended by users
\end{itemize}
\end{block}

To facilitate experimentation,
many solver configurations can be set at runtime; no need to recompile.
It is also possible to control aspects of data layout, choice of computational kernels,
architecture to execute on, and more via the runtime options database. \\
~ \\
The philosophy of supporting extensive runtime experimentation guides many of the 
design choices made in PETSc (including the GPU usage model, which we'll briefly look at if time permits).

\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}{Inexact Newton; Newton-Krylov}

In practice, incurring the expense of an exact solve for the Newton step is often not desirable:\\

{\it Inexact Newton methods} find an approximate Newton direction $\Delta x_k$ that satisfies
\begin{equation}
\| F'(x_k) \Delta x_k + F(x_k) \| \leq \eta \| F(x_k) \|
\end{equation}
for a forcing term $\eta \in [0,1)$ (static or chosen adaptively via Eisenstat-Walker method, {\tt -snes\_ksp\_ew}). \\

Newton-Krylov methods, which use Krylov subspace projection methods as the ``inner'',
linear iterative solver, are a robust and widely-used variant.\\

PETSc provides a wide range of Krylov methods and linear preconditioners that can be
accessed via runtime options ({\tt -ksp\_type <ksp\_method> -pc\_type <pc\_method>}).

\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}{Globalization Strategies}

Newton has quadratic convergence {\it only when the iterate is sufficiently close to the solution}.\\
Far from the solution, the computed Newton step is often too large in magnitude.\\

In practice, some globalization strategy is often needed to expand the domain of convergence.\\
PETSc offers several options; most common (and default) is backtracking line search.

\begin{block}{Backtracking line search}
\begin{itemize}
\item Replaces the full Newton step $s$ with some scalar multiple: $x_{k+1} = x_k + \lambda_k s, \quad \lambda > 0$
\item Introduce merit function $\phi(x) = \frac{1}{2} \|F(x)\|_2^2$, (approximately) find 
$\displaystyle \min_{\lambda>0} \phi(x_k + \lambda s)$
\item Accurate minimization not worth the expense; simply ensure sufficent decrease: 
\begin{equation}
\phi(x_k + \lambda s) \le \phi(x_k) + \alpha \lambda\, s^\top \nabla \phi(x_k)
\end{equation}
($\alpha$ is a user-tunable parameter; defaults to {\tt 1e-4})
\item Builds polynomial model for $\phi(x_k + \lambda s)$ (default is cubic;
  change via {\tt -snes\_linesearch\_order <n>}).
\end{itemize}
\end{block}

\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Slides about how to write code that uses the SNES framework.
\input{slides/SNES/Callbacks.tex}
\input{slides/SNES/Function.tex}
\input{slides/SNES/Jacobian.tex}

% Introduce the driven cavity problem and explore it a little.
\input{slides/SNES/DrivenCavity.tex}
% A thought on the runs: A long options list makes it hard to see what is changing.
% I should have a slide that explains the options we want all the time, and then shows
% how we can put those into the PETSC_OPTIONS environment variable.
% These options should always include "-snes_monitor -snes_converged_reason -ksp_converged_reason".
% "-ksp_converged_reason" important so that we get the number of linear solver iterations.
\input{slides/SNES/ATPESC-DrivenCavityRun_Part1.tex}

% Brief introduction to nonlinear preconditioning and composition.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}{Abstract Nonlinear System and Solver}

To discuss nonlinear composition and preconditioning, we introduce some definitions and notation.\\

Our prototypical nonlinear equation is of the form
\begin{equation}
\label{eqn:NonlinearSystem}
F(x) = b
\end{equation}
and we define the residual as
\begin{equation}
r(x) = F(x) - b
\end{equation}

We use the notation $x_{k+1} = \mathcal{M}(F,x_k,b)$ for the action of a nonlinear solver.

\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}{Nonlinear Composition: Additive}

Nonlinear composition consists of a sequence of two (or more) methods
$\mathcal{M}$ and $\mathcal{N}$, which both provide an approximation solution to
$F(x) = b$.\\
%(\ref{eqn:NonlinearSystem}).\\

In the linear case, application of a stationary solver by defect correct can be written as
\begin{equation}
x_{k+1} = x_k - P^{-1} (A x_k - b)
\end{equation}
where $P^{-1}$ is a linear preconditioner. (Richardson iteration applied to
a preconditioned system.) \\

An additive composition of preconditioners $P^{-1}$ and $Q^{-1}$ with weights $\alpha$ and $\beta$ may be written as

\begin{equation}
x_{k+1} = x_k - (\alpha P^{-1} + \beta Q^{-1})(A x_k - b)
\end{equation}

Analogously, for the nonlinear case, additive composition is

\begin{equation}
x_{k+1} = x_k + \alpha \cdot (\mathcal{M}(F,x_k,b) - x_k) +
          \beta \cdot (\mathcal{N}(F,x_k,b) - x_k)
\end{equation}
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}{Nonlinear Composition: Multiplicative}

A multiplicative combination of linear preconditioners may be written as

\begin{equation}
\begin{aligned}
% I don't use this amsmath environment enough and forget: the &'s tell where to aligtn
x_{k+1/2} &= x_k - P^{-1}(A x_k - b),\\
x_{k+1} &= x_{k+1/2} - Q^{-1} (A x_{k+1/2} - b),
\end{aligned}
\end{equation}

Analogously, for the nonlinear case

\begin{equation}
x_{k+1} = \mathcal{M}(F,\mathcal{N}(F,x_k,b),b)
\end{equation}

which simply indicates to update the solution using the current solution and residual
with the first solver and then update the solution again using the resulting new
solution and new residual with the second solver.
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}{Nonlinear Left Preconditioning}

Recall that the stationary iteration for our left-preconditioned linear system is
\begin{equation}
x_{k+1} = x_k - P^{-1} (A x_k - b)
\end{equation}

And since $A x_k - b = r$, for the linear case we can write the action of our solver
$\mathcal{N}$ as
\begin{equation}
\mathcal{N}(F,x,b) = x_k - P^{-1} r
\end{equation}
With slight rearranging, we can express the left-preconditioned residual
\begin{equation}
P^{-1} r = x_k - \mathcal{N}(F,x,b)
\end{equation}

And generalizing to the nonlinear case, the left preconditioning operation provides a modified residual
\begin{equation}
r_L = x_k - \mathcal{N}(F,x,b)
\end{equation}

\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}{Nonlinear Right Preconditioning}

For a right preconditioned linear system $A P^{-1} P x = b$, we solve the systems
\begin{equation}
\begin{aligned}
A P^{-1} y &= b \\
         x &= P^{-1} y
\end{aligned}
\end{equation}

Analogously, we define the right preconditioning operation in the nonlinear case as
\begin{equation}
\begin{aligned}
y &= \mathcal{M}(F(\mathcal{N}(F,\cdot,b)),x_k,b) \\
x &= \mathcal{N}(F,y,b)
\end{aligned}
\end{equation}

(Note: In the linear case the above actually reduces to
$A (I - P^{-1} A) y = (I - AP^{-1})b$, but the inner solver is applied before
the function evaluation (matrix-vector product in the linear case), so we retain
the ``right preconditioning'' name.)

\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\input{slides/SNES/NPrecTable.tex}

% Show some examples of the utility on nonlinear preconditioning for SNES ex19.
\input{slides/SNES/ATPESC-DrivenCavityRun_Part2.tex}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}{Takeaways}
\small
PETSc provides a wide assortment of nonlinear solvers through the ({\tt SNES}) component\\

Users can build sophisticated solvers from composable algorithmic components:
\begin{itemize}
\item Inner, linear solves can employ full range of solvers and preconditioners provided by PETSc {\tt KSP} and {\tt PC}
  \begin{itemize}
  \item Multigrid solvers particularly important for mesh size-independent convergence
  \end{itemize}
\item Composite nonlinear solvers can be built analogously, using building blocks from PETSc {\tt SNES}
\end{itemize}
~\\
Newton-Krylov dominates, but large design space of ``composed'' nonlinear solvers to explore:
\begin{itemize}
\item Not well-explored theoretically or experimentally (interesting research opportunities!)
\item Composed nonlinear solvers can be very powerful, though frustratingly fragile
  \begin{itemize}
  \item Nonlinear Richardson, Gauss-Seidel, or NGMRES with Newton often improves robustness
  \end{itemize}
\end{itemize}
~ \\
Further items to explore include
\begin{itemize}
\item Nonlinear domain decomposition ({\tt SNESASPIN/SNESNASM}) and nonlinear multigrid (or Full Approximation Scheme, {\tt SNESFAS}) methods
\item PETSc {\tt TS} timesteppers use {\tt SNES} to solve nonlinear problems at each time step
  \begin{itemize}
  \item Pseudo-transient continuation ({\tt TSPSEUDO}) can solve highly nonlinear steady-state problems
  \end{itemize}
\end{itemize}

\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}{Bonus / Extra Credit: Running With GPUs}

Our discussion of nonlinear solver algorithms and how to use them via PETSc is
mostly orthogonal to the topic of how to run PETSc solvers on GPUs. \\
~ \\
Since computing on GPUs has become so important, however, in the slides that
follow, we take a brief look at GPU support in PETSc and how its nonlinear
solvers can be executed on GPUs.
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Taken from Patrick Sanan's 2022 CIG Developer's Workshop talk; updated for my EGU Galileo 11 talk
\begin{frame}[fragile]
\frametitle{How PETSc Uses GPUs: Back-End}
  \begin{itemize}
      \item Provides several new implementations of PETSc's {\tt Vec} (distributed vector) and {\tt Mat} (distributed matrix) classes which allow data storage and manipulation in device (GPU) memory
    \item Embue all \texttt{Vec} (and \texttt{Mat}) objects with the ability to track the state of a second ``offloaded'' copy of the data, and synchronize these two copies of the data (only) when required (``lazy-mirror'' model).
    \item Because higher-level PETSc objects rely on {\tt Vec} and {\tt Mat} operations, execution occurs on GPU when
    appropriate delegated types for {\tt Vec} and {\tt Mat} are chosen.
  \end{itemize}
  \begin{block}{Host and Device Data}
  \begin{lstlisting}[numbers=none]
struct _p_Vec {
  ...
  void             *data;        // host buffer
  void             *spptr;       // device buffer
  PetscOffloadMask offloadmask;  // which copies are valid
};
  \end{lstlisting}
  \end{block}

  \begin{block}{Possible Flag States}
  \begin{lstlisting}
  typedef enum {PETSC_OFFLOAD_UNALLOCATED,
                PETSC_OFFLOAD_GPU,
                PETSC_OFFLOAD_CPU,
                PETSC_OFFLOAD_BOTH} PetscOffloadMask;
  \end{lstlisting}
  \end{block}
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]{Using GPU Back-Ends in PETSc}

Transparently use GPUs for common matrix and vector operations,
via runtime options. \\
Currently CUDA/cuSPARSE, HIP/hipSPARSE, Kokkos, and ViennaCL are supported. %; more may be added.

\medskip
\begin{block}{CUDA/cuSPARSE usage:}
\begin{itemize}
\item CUDA matrix and vector types:\\
{\tt -mat\_type aijcusparse -vec\_type cuda}
%\item Transparently uses GPUs for common matrix and vector operations ---
%no change of user code required.
\item GPU-enabled preconditioners:
  \begin{itemize}
  \item GPU-based ILU: {\tt -pc\_type ilu -pc\_factor\_mat\_solver\_type cusparse}
  \item Jacobi: {\tt -pc\_type jacobi}
  \end{itemize}
\end{itemize}
\end{block}

\medskip
Because PETSc separates high-level control logic from optimized computational kernels,
even very complicated hierarchical/multi-level/domain-decomposed/physics-based solvers can
run on different architectures by simply choosing the appropriate back-end at runtime; 
\textbf{re-coding is not needed}.

%\begin{block}{Kokkos/Kokkos Kernels:}
%\begin{itemize}
%\item Kokkos/Kokkos Kernels matrix and vector types:\\
%{\tt -mat\_type aijkokkos -vec\_type kokkos}
%\item GPU-enabled preconditioners:
%  \begin{itemize}
%  \item GPU-based ILU: {\tt -pc\_type ilu -pc\_factor\_mat\_solver\_type kokkos}
%  \item Jacobi: {\tt -pc\_type jacobi}
%  \end{itemize}
%\item Supports CUDA, HIP, SYCL, OpenMP (choose at build time)
%\end{itemize}
%\end{block}

\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]{Hands-on: CPU and GPU, detailed performance logging}

Run a large version of the driven cavity problem, using multigrid with GPU and SIMD-friendly Chebyshev-Jacobi smoothing ({\tt -mg\_levels\_pc\_type jacobi}), and collect a breakdown by multigrid level ({\tt -pc\_mg\_log}), both in plain text and flamegraph stack formats.
\\ ~ \\
We get the best CPU-only performance on ThetaGPU using 8 MPI ranks:
\begin{lstlisting}
mpiexec -n 8 ./ex19 -da_refine 9 -pc_type mg -mg_levels_pc_type jacobi -pc_mg_log -log_view :log_mg_cpu_n8.txt

mpiexec -n 8 ./ex19 -da_refine 9 -pc_type mg -mg_levels_pc_type jacobi -pc_mg_log -log_view :log_mg_cpu_n8.stack:ascii_flamegraph
\end{lstlisting}

Running on GPU, we get best performance using only one rank \\ (could probably use more if running NVIDIA MPS, but this is not enabled on ThetaGPU):

\begin{lstlisting}
mpiexec -n 1 ./ex19 -da_refine 9 -pc_type mg -mg_levels_pc_type jacobi -pc_mg_log -dm_vec_type cuda -dm_mat_type aijcusparse -log_view_gpu_time -log_view :log_mg_gpu_n1.txt

mpiexec -n 1 ./ex19 -da_refine 9 -pc_type mg -mg_levels_pc_type jacobi -pc_mg_log -dm_vec_type cuda -dm_mat_type aijcusparse -log_view_gpu_time -log_view :log_mg_gpu_n1.stack:ascii_flamegraph
\end{lstlisting}
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% These is Karl's profiling slide with modifications by Richard:
\input{slides/ReadingLogSummary.tex}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[plain,fragile]{PETSc Profiling: Multigrid timings breakdown}

%[basicstyle=\tiny\ttfamily]
{ \tiny
\begin{verbatim}
--- Event Stage 1: MG Apply

BuildTwoSided          2 1.0 8.1144e-05 1.7 0.00e+00 0.0 5.6e+01 4.0e+00 2.0e+00  0  0  0  0  0   0  0  0  0100     0 
MatMult             1872 1.0 1.7854e+01 1.0 1.28e+10 1.0 3.7e+04 4.4e+03 0.0e+00 43 50 51 57  0  74 83 71 89  0  5724 
MatMultAdd           468 1.0 5.8300e-01 1.1 3.69e+08 1.0 6.1e+03 1.7e+03 0.0e+00  1  1  8  4  0   2  2 12  6  0  5057 
MatMultTranspose     468 1.0 7.5539e-01 1.4 3.70e+08 1.0 6.1e+03 1.7e+03 0.0e+00  1  1  8  4  0   3  2 12  6  0  3905 
MatSolve              52 1.0 4.5501e-04 1.2 1.96e+05 1.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0  3452 
MatResidual          468 1.0 4.7088e+00 1.1 3.29e+09 1.0 9.4e+03 4.4e+03 0.0e+00 11 13 13 14  0  19 21 18 22  0  5565 
KSPSolve             988 1.0 1.8321e+01 1.0 1.13e+10 1.0 3.1e+04 4.0e+03 2.0e+00 44 44 42 43  0  75 74 59 67100  4935 
SFSetUp                2 1.0 1.0429e-04 1.4 0.00e+00 0.0 1.1e+02 1.8e+01 2.0e+00  0  0  0  0  0   0  0  0  0100     0 
SFPack              2912 1.0 1.1586e-02 1.4 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0     0 
SFUnpack            2912 1.0 1.4369e-03 3.1 2.44e+05292.8 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0   897
VecCopy             1404 1.0 6.1932e-01 1.2 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  1  0  0  0  0   2  0  0  0  0     0 
VecSet              1560 1.0 2.3179e-01 1.4 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   1  0  0  0  0     0 
VecAYPX             2808 1.0 1.6977e+00 1.3 6.58e+08 1.0 0.0e+00 0.0e+00 0.0e+00  4  3  0  0  0   7  4  0  0  0  3089 
VecAXPBYCZ           936 1.0 9.1505e-01 1.1 8.23e+08 1.0 0.0e+00 0.0e+00 0.0e+00  2  3  0  0  0   4  5  0  0  0  7164 
VecPointwiseMult    1872 1.0 1.8484e+00 1.1 3.29e+08 1.0 0.0e+00 0.0e+00 0.0e+00  4  1  0  0  0   7  2  0  0  0  1419 
VecScatterBegin     2912 1.0 4.0778e-02 1.4 0.00e+00 0.0 5.3e+04 3.5e+03 2.0e+00  0  0 72 64  0   0  0100100100     0 
VecScatterEnd       2912 1.0 1.4528e+00 1.3 2.44e+05292.8 0.0e+00 0.0e+00 0.0e+00  3  0  0  0  0   5  0  0  0  0     1
PCApply             1924 1.0 1.8517e+00 1.1 3.29e+08 1.0 3.0e+03 6.2e+01 2.0e+00  4  1  4  0  0   8  2  6  0100  1417 
MGSmooth Level 0      52 1.0 1.8742e-03 1.1 1.96e+05 1.0 3.0e+03 6.2e+01 2.0e+00  0  0  4  0  0   0  0  6  0100   838 
MGSmooth Level 1     104 1.0 1.6707e-03 1.2 2.20e+05 3.3 3.1e+03 9.0e+01 0.0e+00  0  0  4  0  0   0  0  6  0  0   758 
MGResid Level 1       52 1.0 4.5318e-04 1.6 6.32e+04 3.5 1.0e+03 9.0e+01 0.0e+00  0  0  1  0  0   0  0  2  0  0   797 
MGInterp Level 1     104 1.0 1.1601e-03 1.2 1.54e+04 3.7 1.4e+03 4.7e+01 0.0e+00  0  0  2  0  0   0  0  3  0  0    75 
MGSmooth Level 2     104 1.0 2.3559e-03 1.4 7.49e+05 1.6 3.1e+03 1.7e+02 0.0e+00  0  0  4  0  0   0  0  6  0  0  1949 
MGResid Level 2       52 1.0 8.1213e-04 2.3 2.15e+05 1.6 1.0e+03 1.7e+02 0.0e+00  0  0  1  0  0   0  0  2  0  0  1625 
MGInterp Level 2     104 1.0 9.9738e-04 1.2 5.03e+04 1.7 1.4e+03 7.6e+01 0.0e+00  0  0  2  0  0   0  0  3  0  0   308 
MGSmooth Level 3     104 1.0 5.0022e-03 1.9 2.51e+06 1.3 3.1e+03 3.2e+02 0.0e+00  0  0  4  0  0   0  0  6  1  0  3487 
MGResid Level 3       52 1.0 2.3317e-03 4.2 7.24e+05 1.3 1.0e+03 3.2e+02 0.0e+00  0  0  1  0  0   0  0  2  0  0  2159 
MGInterp Level 3     104 1.0 1.1775e-03 1.2 1.61e+05 1.2 1.4e+03 1.4e+02 0.0e+00  0  0  2  0  0   0  0  3  0  0   977 
MGSmooth Level 4     104 1.0 1.8681e-02 2.3 9.14e+06 1.1 3.1e+03 6.3e+02 0.0e+00  0  0  4  1  0   0  0  6  1  0  3637 
MGResid Level 4       52 1.0 6.5990e-03 3.8 2.64e+06 1.1 1.0e+03 6.3e+02 0.0e+00  0  0  1  0  0   0  0  2  0  0  2978 
MGInterp Level 4     104 1.0 1.8010e-03 1.3 5.89e+05 1.1 1.4e+03 2.5e+02 0.0e+00  0  0  2  0  0   0  0  3  0  0  2474 
MGSmooth Level 5     104 1.0 5.9776e-02 1.6 3.48e+07 1.1 3.1e+03 1.2e+03 0.0e+00  0  0  4  1  0   0  0  6  2  0  4486 
MGResid Level 5       52 1.0 2.1561e-02 2.6 1.01e+07 1.1 1.0e+03 1.2e+03 0.0e+00  0  0  1  0  0   0  0  2  1  0  3601 
MGInterp Level 5     104 1.0 5.2430e-03 1.8 2.26e+06 1.0 1.4e+03 4.9e+02 0.0e+00  0  0  2  0  0   0  0  3  0  0  3344 
[...]
\end{verbatim}
}
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]{Additional Performance Logging Features: GPU}

\begin{block}{GPU logging}
\begin{itemize}
\item When running with GPUs, get additional columns recording GPU flops and data transfers
\item Use {\tt -log\_view\_gpu\_time} to get GPU logging for all events (incurs some expense)
\end{itemize}
\begin{lstlisting}[language=]
------------------ ... ---------------------------------------
Event              ...  GPU    - CpuToGpu -   - GpuToCpu - GPU
                   ... Mflop/s Count   Size   Count   Size  %F
------------------ ... ---------------------------------------

...
KSPSetUp           ... 82806    112 1.00e+03   35 6.30e+01 100
KSPSolve           ... 119068    65 1.10e-01   65 1.15e-01 100
KSPGMRESOrthog     ... 135451     0 0.00e+00    0 0.00e+00 100 
SNESSolve          ... 111924  1241 2.75e+03  631 9.30e+02  91
SNESSetUp          ...     0      0 0.00e+00    0 0.00e+00  0 
SNESFunctionEval   ...     0      7 3.79e+01    4 3.80e+01  0
SNESJacobianEval   ...     0   1022 1.59e+03  527 8.29e+02  0
SNESLineSearch     ... 18807      6 5.68e+01    3 2.85e+01  71
...
\end{lstlisting}
\end{block}

\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
{
\usebackgroundtemplate{\parbox[b][\paperheight][b]{\paperwidth}{}}
\begin{frame}[plain,fragile]{SNES ex19 CPU vs. GPU flame graph comparison }
\includegraphics[width=\textwidth]{figures/side-by-side-flamegraphs}

Download the *.stack files to your local machine and then use
\url{https://www.speedscope.app} to generate flame graphs,
which will let you examine (interactively) the hierarchy of PETSc events.
\\ ~ \\
One thing to note is the relative distribution of time in {\tt MGSmooth} steps (part of {\tt PCApply}). 
See how the steps on the coarse levels all take roughly the same time on the GPU?
This points to the high kernel launch latency. What other noteworthy differences can you find?

\end{frame}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}{Further things to try}

\begin{block}{Speedup (or slowdown!) for GPU vs.\ CPU}
Total time in {\tt SNESSolve} tells us the time required to solve our entire problem.
Compare these for the CPU and GPU cases to get the overall speedup, but what parts
sped up in the GPU case? Which parts actually slowed down?
(The slowdowns are mostly due to the fact that the nonlinear function and Jacobian routines in
SNES ex19 do not run on the GPU---verify this by looking at the {\tt GPU \%F} column in the
text version of the log---and we had to use fewer ranks in this case. See
SNES tutorial ex55 in the main development branch of PETSc for an example where
these run on the GPU.)
\\~\\
If you'd like to try another GPU-back end, you can try PETSc's Kokkos/Kokkos Kernels one.
Run with {\tt -dm\_mat\_type aijkokkos -dm\_vec\_type kokkos}.
\end{block}

\begin{block}{Experimenting with different multigrid cycle types}
Our SNES ex19 runs defaulted to using multigrid V-cycles.
Try running with W-cycles instead by using the option {\tt -pc\_mg\_cycle\_type w}.
Unlike V-cycles, W-cycles visit coarse levels many more times than fine ones.
What does this do to the time spent in multigrid smoothers for the GPU case vs. the CPU-only one?
Should one or the other of these be favored when using GPUs?
\end{block}

\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\end{document}

