%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[t,fragile]{Hands-on: Running the driven cavity}

\vspace{.25in}
Run SNES ex19 with a single MPI rank (see full instructions for all hands-on exercises \magenta{\href{https://xsdk-project.github.io/MathPackagesTraining2022/lessons/nonlinear_solvers_petsc/}{here}}):
\begin{lstlisting}
./ex19 -snes_monitor -snes_converged_reason -da_grid_x 16 -da_grid_y 16 -da_refine 2 -lidvelocity 100 -grashof 1e2
\end{lstlisting}

\only<2>{
Explanation of the above command-line options:
{ \footnotesize
\begin{itemize}
\item {\tt -snes\_monitor}: Show progress of the SNES solver
\item {\tt -snes\_converged\_reason}: Print reason for SNES convergence or divergence
\item {\tt -da\_grid\_x 16}: Set initial grid points in x direction to 16
\item {\tt -da\_grid\_y 16}: Set initial grid poings in y direction to 16
\item {\tt -da\_refine 2}: Refine the initial grid 2 times before creation
\item {\tt -lidvelocity 100}: Set dimensionless velocity of lid to 100
\item {\tt -grashof 1e2}: Set Grashof number to 1e2
\end{itemize} }

An element of the PETSc design philosophy is extensive runtime customizability;\\
Use {\tt -help} to enumerate and explain the various command-line options available.
}

\begin{onlyenv}<3> % Can't put lstlisting inside \only<n>{}; need to use 'onlyenv'.
\begin{lstlisting}
lid velocity = 100., prandtl # = 1., grashof # = 100.
  0 SNES Function norm 7.681163231938e+02 
  1 SNES Function norm 6.582880149343e+02 
  2 SNES Function norm 5.294044874550e+02 
  3 SNES Function norm 3.775102116141e+02 
  4 SNES Function norm 3.047226778615e+02 
  5 SNES Function norm 2.599983722908e+00 
  6 SNES Function norm 9.427314747057e-03 
  7 SNES Function norm 5.212213461756e-08 
Nonlinear solve converged due to CONVERGED_FNORM_RELATIVE iterations 7
Number of SNES iterations = 7
\end{lstlisting}
\end{onlyenv}
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]{What is the SNES solver actually doing? Add {\tt -snes\_view} to see}
\tiny
\begin{lstlisting}[basicstyle=\ttfamily \tiny, language=]
SNES Object: 1 MPI processes
  type: newtonls
  maximum iterations=50, maximum function evaluations=10000
  tolerances: relative=1e-08, absolute=1e-50, solution=1e-08
  total number of linear solver iterations=835
  total number of function evaluations=11
  norm schedule ALWAYS
  Jacobian is built using colored finite differences on a DMDA
  SNESLineSearch Object: 1 MPI processes
    type: bt
      interpolation: cubic
      alpha=1.000000e-04
    maxstep=1.000000e+08, minlambda=1.000000e-12
    tolerances: relative=1.000000e-08, absolute=1.000000e-15, lambda=1.000000e-08
    maximum iterations=40
  KSP Object: 1 MPI processes
    type: gmres
      restart=30, using Classical (unmodified) Gram-Schmidt Orthogonalization with no iterative refinement
      happy breakdown tolerance 1e-30
    maximum iterations=10000, initial guess is zero
    tolerances:  relative=1e-05, absolute=1e-50, divergence=10000.
    left preconditioning
    using PRECONDITIONED norm type for convergence test
  PC Object: 1 MPI processes
    type: ilu
      out-of-place factorization
      0 levels of fill
      tolerance for zero pivot 2.22045e-14
      matrix ordering: natural
      factor fill ratio given 1., needed 1.
        Factored matrix follows:
          Mat Object: 1 MPI processes
            type: seqaij
            rows=14884, cols=14884, bs=4
            package used to perform factorization: petsc
            total: nonzeros=293776, allocated nonzeros=293776
                                    [continued...]
\end{lstlisting}
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]{Managing PETSc options}
PETSc offers a very large number of runtime options.\\
All can be set via command line, but can also be set from input files
and shell environment variables. \\

To facilitate readability, we'll put the command-line arguments common to the remaining hands-on exercises
in {\tt PETSC\_OPTIONS}.
\begin{lstlisting}[basicstyle=\ttfamily \footnotesize,language=]
export PETSC_OPTIONS="-snes_monitor -snes_converged_reason -lidvelocity 100 -da_grid_x 16 -da_grid_y 16 -ksp_converged_reason -log_view :log.txt"
\end{lstlisting}
We've added {\tt -ksp\_converged\_reason} to see how and when linear solver halts. \\

We've also added {\tt -log\_view} to write the PETSc performance logging info to a file.\\
We don't have time to explain the performance logs; find the overall wall-clock time via
\begin{lstlisting}
grep Time\ \(sec\): log.txt
\end{lstlisting}

\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]{Hands-on: Exact vs.\ Inexact Newton}

PETSc defaults to inexact Newton. To run exact (and check the execution time), do

\begin{lstlisting}
./ex19 -da_refine 2 -grashof 1e2 -pc_type lu
grep Time\ \(sec\): log.txt
\end{lstlisting}

Now run inexact Newton and vary the liner solve tolerance ({\tt -ksp\_rtol}).
\begin{lstlisting}
./ex19 -da_refine 2 -grashof 1e2 -ksp_rtol 1e-8
./ex19 -da_refine 2 -grashof 1e2 -ksp_rtol 1e-5
./ex19 -da_refine 2 -grashof 1e2 -ksp_rtol 1e-3
./ex19 -da_refine 2 -grashof 1e2 -ksp_rtol 1e-2
./ex19 -da_refine 2 -grashof 1e2 -ksp_rtol 1e-1
\end{lstlisting}
What happens to the SNES iteration count? When does it diverge? \\
What yields the shortest execution time?
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[t,fragile]{Hands-on: Scaling up grid size and running in parallel}
% The 't' argument above top-aligns things; this keeps text from jumping around in the frame as we go through the
% different 'onlyenv' overlays.

What happens to iteration counts (and execution time) as we scale up the grid size?

For this exercise, run in parallel because experiments may take too long otherwise. \\
We also use BiCGStab ({\tt -ksp\_type bcgs}) because the default GMRES(30) fails for some cases.\\

\begin{onlyenv}<2,3>
Run with default preconditioner. What happens to iteration counts and execution time?
\begin{lstlisting}[basicstyle=\ttfamily \footnotesize,language=]
mpiexec -n 12 ./ex19 -ksp_type bcgs -grashof 1e2 -da_refine 2
mpiexec -n 12 ./ex19 -ksp_type bcgs -grashof 1e2 -da_refine 3
mpiexec -n 12 ./ex19 -ksp_type bcgs -grashof 1e2 -da_refine 4 
\end{lstlisting}
\end{onlyenv}

\begin{onlyenv}<3>
\begin{lstlisting}[basicstyle=\ttfamily \scriptsize,language=]
$ mpiexec -n 12 ./ex19 -ksp_type bcgs -grashof 1e2 -da_refine 4 
lid velocity = 100., prandtl # = 1., grashof # = 100.
  0 SNES Function norm 1.545962539057e+03 
  Linear solve converged due to CONVERGED_RTOL iterations 172
  1 SNES Function norm 9.780980584978e+02 
  Linear solve converged due to CONVERGED_RTOL iterations 128
  2 SNES Function norm 6.620854219003e+02 
  Linear solve converged due to CONVERGED_RTOL iterations 600
  3 SNES Function norm 3.219025282761e+00 
  Linear solve converged due to CONVERGED_RTOL iterations 470
  4 SNES Function norm 9.280944447516e-03 
  Linear solve converged due to CONVERGED_RTOL iterations 467
  5 SNES Function norm 1.354460792476e-07 
Nonlinear solve converged due to CONVERGED_FNORM_RELATIVE iterations 5
\end{lstlisting}
\end{onlyenv}

\begin{onlyenv}<4,5>
%SNES iteration count doesn't grow, but linear iteration count grows quickly with grid size.\\
Let's try geometric multigrid (defaults to V-cycle) by adding {\tt -pc\_type mg}
\begin{lstlisting}[basicstyle=\ttfamily \footnotesize,language=]
mpiexec -n 12 ./ex19 -ksp_type bcgs -grashof 1e2 -pc_type mg -da_refine 2
mpiexec -n 12 ./ex19 -ksp_type bcgs -grashof 1e2 -pc_type mg -da_refine 3
mpiexec -n 12 ./ex19 -ksp_type bcgs -grashof 1e2 -pc_type mg -da_refine 4 
\end{lstlisting}
\end{onlyenv}

\begin{onlyenv}<5>
\begin{lstlisting}[basicstyle=\ttfamily \scriptsize,language=]
mpiexec -n 12 ./ex19 -ksp_type bcgs -grashof 1e2 -pc_type mg -da_refine 4
lid velocity = 100., prandtl # = 1., grashof # = 100.
  0 SNES Function norm 1.545962539057e+03 
  Linear solve converged due to CONVERGED_RTOL iterations 6
  1 SNES Function norm 9.778196290981e+02 
  Linear solve converged due to CONVERGED_RTOL iterations 6
  2 SNES Function norm 6.609659458090e+02 
  Linear solve converged due to CONVERGED_RTOL iterations 7
  3 SNES Function norm 2.791922927549e+00 
  Linear solve converged due to CONVERGED_RTOL iterations 6
  4 SNES Function norm 4.973591997243e-03 
  Linear solve converged due to CONVERGED_RTOL iterations 6
  5 SNES Function norm 3.241555827567e-05 
  Linear solve converged due to CONVERGED_RTOL iterations 9
  6 SNES Function norm 9.883136583477e-10 
Nonlinear solve converged due to CONVERGED_FNORM_RELATIVE iterations 6
\end{lstlisting}
\end{onlyenv}
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[t,fragile]{Hands-on: Increasing strength of the nonlinearity}

What happens as we increase the nonlinearity by raising the Grashof number?

\begin{onlyenv}<1,2>
\begin{lstlisting}[basicstyle=\ttfamily \footnotesize,language=]
./ex19 -da_refine 2 -grashof 1e2
./ex19 -da_refine 2 -grashof 1e3
./ex19 -da_refine 2 -grashof 1e4
./ex19 -da_refine 2 -grashof 1.3e4
\end{lstlisting}
\end{onlyenv}

\begin{onlyenv}<2>
\begin{lstlisting}[basicstyle=\ttfamily \footnotesize,language=]
./ex19 -da_refine 2 -grashof 1.3e4
lid velocity = 100., prandtl # = 1., grashof # = 13000.
  0 SNES Function norm 7.971152173639e+02 
  Linear solve did not converge due to DIVERGED_ITS iterations 10000
Nonlinear solve did not converge due to DIVERGED_LINEAR_SOLVE iterations 0
\end{lstlisting}

Oops! Failure in the linear solver? What if we use a stronger preconditioner?
\end{onlyenv}

\begin{onlyenv}<3,4>
\begin{lstlisting}
./ex19 -da_refine 2 -grashof 1e2
./ex19 -da_refine 2 -grashof 1e3
./ex19 -da_refine 2 -grashof 1e4
./ex19 -da_refine 2 -grashof 1.3e4
./ex19 -da_refine 2 -grashof 1.3e4 -pc_type mg
\end{lstlisting}
\end{onlyenv}

\begin{onlyenv}<4>
\begin{lstlisting}[basicstyle=\ttfamily \footnotesize,language=]
./ex19 -da_refine 2 -grashof 1.3e4 -pc_type mg
lid velocity = 100., prandtl # = 1., grashof # = 13000.
  ...
  4 SNES Function norm 3.209967262833e+02 
  Linear solve converged due to CONVERGED_RTOL iterations 9
  5 SNES Function norm 2.121900163587e+02 
  Linear solve converged due to CONVERGED_RTOL iterations 9
  6 SNES Function norm 1.139162432910e+01 
  Linear solve converged due to CONVERGED_RTOL iterations 8
  7 SNES Function norm 4.048269317796e-01 
  Linear solve converged due to CONVERGED_RTOL iterations 8
  8 SNES Function norm 3.264993685206e-04 
  Linear solve converged due to CONVERGED_RTOL iterations 8
  9 SNES Function norm 1.154893029612e-08 
Nonlinear solve converged due to CONVERGED_FNORM_RELATIVE iterations 9
\end{lstlisting}
\end{onlyenv}

\begin{onlyenv}<5,6>
\begin{lstlisting}[basicstyle=\ttfamily \footnotesize,language=]
./ex19 -da_refine 2 -grashof 1e2
./ex19 -da_refine 2 -grashof 1e3
./ex19 -da_refine 2 -grashof 1e4
./ex19 -da_refine 2 -grashof 1.3e4
./ex19 -da_refine 2 -grashof 1.3e4 -pc_type mg
./ex19 -da_refine 2 -grashof 1.3373e4 -pc_type mg
\end{lstlisting}
\end{onlyenv}

\begin{onlyenv}<6>
\begin{lstlisting}[basicstyle=\ttfamily \footnotesize,language=]
lid velocity = 100., prandtl # = 1., grashof # = 13373.
...
 48 SNES Function norm 3.124919801005e+02 
  Linear solve converged due to CONVERGED_RTOL iterations 17
 49 SNES Function norm 3.124919800338e+02 
  Linear solve converged due to CONVERGED_RTOL iterations 17
 50 SNES Function norm 3.124919799645e+02 
Nonlinear solve did not converge due to DIVERGED_MAX_IT iterations 50
\end{lstlisting}
\end{onlyenv}

\begin{onlyenv}<7,8>
\begin{lstlisting}[basicstyle=\ttfamily \footnotesize,language=]
./ex19 -da_refine 2 -grashof 1e2
./ex19 -da_refine 2 -grashof 1e3
./ex19 -da_refine 2 -grashof 1e4
./ex19 -da_refine 2 -grashof 1.3e4
./ex19 -da_refine 2 -grashof 1.3e4 -pc_type mg
./ex19 -da_refine 2 -grashof 1.3373e4 -pc_type mg
./ex19 -da_refine 2 -grashof 1.3373e4 -pc_type lu
\end{lstlisting}
\end{onlyenv}

\begin{onlyenv}<8>
\begin{lstlisting}[basicstyle=\ttfamily \footnotesize,language=]
./ex19 -da_refine 2 -grashof 1.3373e4 -pc_type lu
...
 48 SNES Function norm 3.193724239842e+02 
  Linear solve converged due to CONVERGED_RTOL iterations 1
 49 SNES Function norm 3.193724232621e+02 
  Linear solve converged due to CONVERGED_RTOL iterations 1
 50 SNES Function norm 3.193724181714e+02 
Nonlinear solve did not converge due to DIVERGED_MAX_IT iterations 50
\end{lstlisting}

A strong linear solver can't help us here. What now?\\

Let's try combining Newton's method with one of the other nonlinear solvers we mentioned in the introduction,
using PETSc's support for nonlinear composition and preconditioning.
\end{onlyenv}

\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
