\documentclass[aspectratio=169,9pt]{beamer}

\include{include/header}
\include{include/headerLocal}
\usepackage{JedMacros-modified}
\usepackage{comment}
\newcommand{\M}{\mathcal{M}}
\newcommand{\pc}{\ensuremath{-}}
\newcommand{\lp}{\ensuremath{\pc_{L}}}
\newcommand{\rp}{\ensuremath{\pc_{R}}}
\newcommand{\lin}{\ensuremath{\backslash}}
\newcommand{\solvername}[1]{\ensuremath{\begingroup\text{#1}\endgroup}}
\newcommand{\krylov}{\solvername{K}}
\newcommand{\NEWT}{\mathcal{N}}

\lstloadlanguages{C++}
\lstset{%
  language=C++,
  basicstyle=\footnotesize\ttfamily,
  commentstyle=\itshape\color{darkgreen},
  keywordstyle=\bfseries\color{darkblue},
  stringstyle=\color{darkred},
  showspaces=false,
  showtabs=false,
  columns=fixed,
%  backgroundcolor=\color{lightgrey},
  numbers=none,
  frame=single,
  numberstyle=\tiny,
  breaklines=true,
  breakatwhitespace=true, % Need this so we don't break command lines options
  showstringspaces=false,
  xleftmargin=0.1cm,
  xrightmargin=3em
}%

\lstset{emphstyle=\color{red}}

\date{August 4, 2020}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{document}
\setitemize{topsep=2pt,partopsep=0pt,parsep=0pt,leftmargin=*,label=\textbullet}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% ... Title Page
\setbeamertemplate{footline}{}
{

\usebackgroundtemplate{\parbox[b][\paperheight][b]{\paperwidth}{\includegraphics[page=1,width=\paperwidth]{ATPESC_template}}}
\begin{frame}[t]{}
\begin{textblock}{80}(4.4,15)
{ \huge \bf
Introduction to Nonlinear Solvers Using PETSc/TAO }
\end{textblock}

\begin{textblock}{80}(4.4,30)
Presented to\\
{\bf ATPESC 2020 Participants}
\end{textblock}

\begin{textblock}{80}(4.4,48)
Richard Tran Mills \\
Argonne National Laboratory \\
August 4, 2020

\vspace{0.01in}
\hspace{-0.12in}
\includegraphics[width=0.30\linewidth]{figures/PETSc-TAO_RGB.png}

\end{textblock}

%\hspace{-.1in}

\end{frame}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\setbeamertemplate{footline}[frame number]{}
\setbeamertemplate{footline}[text line]{%
  \parbox{\linewidth}{\vspace{-3em} \insertpagenumber ~ ATPESC 2020, July 26 -- August 7, 2020}}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}{Iterative Solvers for Nonlinear Systems}
% Note that, unlike linear systems, one cannot in general solve nonlinear systems in a finite number of steps:
%  Iterative methods required!
% Introduce basic iteration (Richardson), extensions to nonlinear Krylov-like methods.

Systems of nonlinear equations 

\begin{equation}
F(x) = b \quad \mathrm{where} \quad F : \R^N \to \R^N
\end{equation}

arise in countless settings in computational science.\\
Direct methods for general nonlinear systems do not exist.
Iterative methods are required!

%%%
\begin{block}{Nonlinear Richardson (simple) iteration:}

\begin{equation}
x_{k+1} = x_k + \lambda (b - F(x_k))
\end{equation}

This has linear convergence at best: $\|e_{k+1}\| \leq C \|e_k\|$
\end{block}
%%%

%%%
\begin{block}{Nonlinear Krylov methods}
\textcolor{red}{Nonlinear CG} - Mimic CG to force each new search direction to be orthogonal to previous directions.

\textcolor{red}{Nonlinear GMRES (Anderson mixing)} - minimize $\|F(x_{k+1} - b)\|$ by using $x_{k+1}$ as a linear combination of previous
solutions and solving a linear least squares problem.

These have superlinear convergence at best: $\|e_{k+1}\| \leq C \|e_k\|^{\alpha \geq 1}$
\end{block}

\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\input{slides/SNES/SNESNewton.tex}
% Need to talk about globalization (line search is the instance we'll discuss).
% From http://homepages.rpi.edu/home/37/hickej2/yesterday/public_html/papers/AIAA-2009-4139.pdf:
% "globalization seeks to move arbitrary initial iterates into the basin of attraction of Newton's method"
% Also need a slide on inexact Newton and Newton-Krylov.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}{\Huge High-Performance Nonlinear Solvers Are Available Via}

\begin{minipage}[t]{0.3 \textwidth}
\includegraphics[width=\linewidth]{figures/PETSc-TAO_RGB.png}
\end{minipage} \\
%\begin{minipage}[t]{0.66 \textwidth}
\href{https://www.mcs.anl.gov/petsc/}{\blue{PETSc: Portable, Extensible Toolkit for Scientific Computation}} \\
%\end{minipage}
~ \\

\begin{minipage}[t]{0.1 \textwidth}
\includegraphics[width=\linewidth]{figures/sundials_logo_higher_contrast.png}
\end{minipage}
\begin{minipage}[t]{0.88 \textwidth}
\vspace{-.32in}
\href{https://computing.llnl.gov/projects/sundials}{\blue{SUNDIALS: SUite of Nonlinear and DIfferential/ALgebraic Equation Solvers}} \\
\end{minipage} \\
~ \\

\begin{minipage}[t]{0.1 \textwidth}
\includegraphics[width= \linewidth]{figures/trilinos.jpg}
\end{minipage}
\begin{minipage}[t]{0.88 \textwidth}
\vspace{-.27in}
Trilinos: \href{https://trilinos.github.io/nox_and_loca.html}{\blue{NOX (Nonlinear Object-Oriented Solutions) and \\ LOCA (Library of Continuation Algorithms)}} packages. \\
\end{minipage} \\
~ \\

{\Large \bf This presentation focuses on PETSc.}

\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\usebackgroundtemplate{\parbox[b][\paperheight][b]{\paperwidth}{\includegraphics[page=1,width=\paperwidth]{powerpoint/petsc-overview}}}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\usebackgroundtemplate{\parbox[b][\paperheight][b]{\paperwidth}{\includegraphics[page=2,width=\paperwidth]{powerpoint/petsc-overview}}}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%% Reset things back to the normal template we are using,
%%% after using some background template trickery to include PDF versions of PowerPoint slides.
\usebackgroundtemplate{\parbox[b][\paperheight][b]{\paperwidth}{\includegraphics[page=2,width=\paperwidth]{ATPESC_template}}}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}{\Huge PETSc is a platform for experimentation!}

Optimal solvers must consider the interplay among physics, algorithms, and architectures.\\
This need motivates several elements of the PETSc design philosophy:

\begin{block}{Algebraic solvers must be}
\begin{itemize}
\item Composable: Separately developed solvers should be easy to combine, by
non-experts, to form a more powerful solver.
\item Hierarchical: Outer solvers may iterate over all variables for a global
problem, while inner solvers handle smaller subsets of physics, smaller
physical subdomains, or coarser meshes.
\item Nested: Outer solvers call nested inner solvers
\item Extensible: Easily customized or extended by users
\end{itemize}
\end{block}

To facilitate experimentation,
many solver configurations can be set at runtime; no need to recompile.
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}{Inexact Newton; Newton-Krylov}

In practice, incurring the expense of an exact solve for the Newton step is often not desirable:\\

{\it Inexact Newton methods} find an approximate Newton direction $\Delta x_k$ that satisfies
\begin{equation}
\| F'(x_k) \Delta x_k + F(x_k) \| \leq \eta \| F(x_k) \|
\end{equation}
for a forcing term $\eta \in [0,1)$ (static or chosen adaptively via Eisenstat-Walker method, {\tt -snes\_ksp\_ew}). \\

Newton-Krylov methods, which use Krylov subspace projection methods as the ``inner'',
linear iterative solver, are a robust and widely-used variant.\\

PETSc provides a wide range of Krylov methods and linear preconditioners that can be
accessed via runtime options ({\tt -ksp\_type <ksp\_method> -pc\_type <pc\_method>}).

\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}{Globalization Strategies}

Newton has quadratic convergence {\it only when the iterate is sufficiently close to the solution}.\\
Far from the solution, the computed Newton step is often too large in magnitude.\\

In practice, some globalization strategy is often needed to expand the domain of convergence.\\
PETSc offers several options; most common (and default) is backtracking line search.

\begin{block}{Backtracking line search}
\begin{itemize}
\item Replaces the full Newton step $s$ with some scalar multiple: $x_{k+1} = x_k + \lambda_k s, \quad \lambda > 0$
\item Introduce merit function $\phi(x) = \frac{1}{2} \|F(x)\|_2^2$, (approximately) find 
$\displaystyle \min_{\lambda>0} \phi(x_k + \lambda s)$
\item Accurate minimization not worth the expense; simply ensure sufficent decrease: 
\begin{equation}
\phi(x_k + \lambda s) \le \phi(x_k) + \alpha \lambda\, s^\top \nabla \phi(x_k)
\end{equation}
($\alpha$ is a user-tunable parameter; defaults to {\tt 1e-4})
\item Builds polynomial model for $\phi(x_k + \lambda s)$ (default is cubic;
  change via {\tt -snes\_linesearch\_order <n>}).
\end{itemize}
\end{block}

\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Slides about how to write code that uses the SNES framework.
\input{slides/SNES/Callbacks.tex}
\input{slides/SNES/Function.tex}
\input{slides/SNES/Jacobian.tex}

% Introduce the driven cavity problem and explore it a little.
\input{slides/SNES/DrivenCavity.tex}
% A thought on the runs: A long options list makes it hard to see what is changing.
% I should have a slide that explains the options we want all the time, and then shows
% how we can put those into the PETSC_OPTIONS environment variable.
% These options should always include "-snes_monitor -snes_converged_reason -ksp_converged_reason".
% "-ksp_converged_reason" important so that we get the number of linear solver iterations.
\input{slides/SNES/ATPESC-DrivenCavityRun_Part1.tex}

% Brief introduction to nonlinear preconditioning and composition.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}{Abstract Nonlinear System and Solver}

To discuss nonlinear composition and preconditioning, we introduce some definitions and notation.\\

Our prototypical nonlinear equation is of the form
\begin{equation}
\label{eqn:NonlinearSystem}
F(x) = b
\end{equation}
and we define the residual as
\begin{equation}
r(x) = F(x) - b
\end{equation}

We use the notation $x_{k+1} = \mathcal{M}(F,x_k,b)$ for the action of a nonlinear solver.

\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}{Nonlinear Composition: Additive}

Nonlinear composition consists of a sequence of two (or more) methods
$\mathcal{M}$ and $\mathcal{N}$, which both provide an approximation solution to
$F(x) = b$.\\
%(\ref{eqn:NonlinearSystem}).\\

In the linear case, application of a stationary solver by defect correct can be written as
\begin{equation}
x_{k+1} = x_k - P^{-1} (A x_k - b)
\end{equation}
where $P^{-1}$ is a linear preconditioner. (Richardson iteration applied to
a preconditioned system.) \\

An additive composition of preconditioners $P^{-1}$ and $Q^{-1}$ with weights $\alpha$ and $\beta$ may be written as

\begin{equation}
x_{k+1} = x_k - (\alpha P^{-1} + \beta Q^{-1})(A x_k - b)
\end{equation}

Analogously, for the nonlinear case, additive composition is

\begin{equation}
x_{k+1} = x_k + \alpha \cdot (\mathcal{M}(F,x_k,b) - x_k) +
          \beta \cdot (\mathcal{N}(F,x_k,b) - x_k)
\end{equation}
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}{Nonlinear Composition: Multiplicative}

A multiplicative combination of linear preconditioners may be written as

\begin{equation}
\begin{aligned}
% I don't use this amsmath environment enough and forget: the &'s tell where to aligtn
x_{k+1/2} &= x_k - P^{-1}(A x_k - b),\\
x_{k+1} &= x_{k+1/2} - Q^{-1} (A x_{k+1/2} - b),
\end{aligned}
\end{equation}

Analogously, for the nonlinear case

\begin{equation}
x_{k+1} = \mathcal{M}(F,\mathcal{N}(F,x_k,b),b)
\end{equation}

which simply indicates to update the solution using the current solution and residual
with the first solver and then update the solution again using the resulting new
solution and new residual with the second solver.
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}{Nonlinear Left Preconditioning}

Recall that the stationary iteration for our left-preconditioned linear system is
\begin{equation}
x_{k+1} = x_k - P^{-1} (A x_k - b)
\end{equation}

And since $A x_k - b = r$, for the linear case we can write the action of our solver
$\mathcal{N}$ as
\begin{equation}
\mathcal{N}(F,x,b) = x_k - P^{-1} r
\end{equation}
With slight rearranging, we can express the left-preconditioned residual
\begin{equation}
P^{-1} r = x_k - \mathcal{N}(F,x,b)
\end{equation}

And generalizing to the nonlinear case, the left preconditioning operation provides a modified residual
\begin{equation}
r_L = x_k - \mathcal{N}(F,x,b)
\end{equation}

\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}{Nonlinear Right Preconditioning}

For a right preconditioned linear system $A P^{-1} P x = b$, we solve the systems
\begin{equation}
\begin{aligned}
A P^{-1} y &= b \\
         x &= P^{-1} y
\end{aligned}
\end{equation}

Analogously, we define the right preconditioning operation in the nonlinear case as
\begin{equation}
\begin{aligned}
y &= \mathcal{M}(F(\mathcal{N}(F,\cdot,b)),x_k,b) \\
x &= \mathcal{N}(F,y,b)
\end{aligned}
\end{equation}

(Note: In the linear case the above actually reduces to
$A (I - P^{-1} A) y = (I - AP^{-1})b$, but the inner solver is applied before
the function evaluation (matrix-vector product in the linear case), so we retain
the ``right preconditioning'' name.)

\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\input{slides/SNES/NPrecTable.tex}

% Show some examples of the utility on nonlinear preconditioning for SNES ex19.
\input{slides/SNES/ATPESC-DrivenCavityRun_Part2.tex}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}{Takeaways}
\small
PETSc provides a wide assortment of nonlinear solvers through the ({\tt SNES}) component\\

Users can build sophisticated solvers from composable algorithmic components:
\begin{itemize}
\item Inner, linear solves can employ full range of solvers and preconditioners provided by PETSc {\tt KSP} and {\tt PC}
  \begin{itemize}
  \item Multigrid solvers particularly important for mesh size-independent convergence
  \end{itemize}
\item Composite nonlinear solvers can be built analogously, using building blocks from PETSc {\tt SNES}
\end{itemize}
~\\
Newton-Krylov dominates, but large design space of ``composed'' nonlinear solvers to explore:
\begin{itemize}
\item Not well-explored theoretically or experimentally (interesting research opportunities!)
\item Composed nonlinear solvers can be very powerful, though frustratingly fragile
  \begin{itemize}
  \item Nonlinear Richardson, Gauss-Seidel, or NGMRES with Newton often improves robustness
  \end{itemize}
\end{itemize}
~ \\
Further items to explore include
\begin{itemize}
\item Nonlinear domain decomposition ({\tt SNESASPIN/SNESNASM}) and nonlinear multigrid (or Full Approximation Scheme, {\tt SNESFAS}) methods
\item PETSc {\tt TS} timesteppers use {\tt SNES} to solve nonlinear problems at each time step
  \begin{itemize}
  \item Pseudo-transient continuation ({\tt TSPSEUDO}) can solve highly nonlinear steady-state problems
  \end{itemize}
\end{itemize}

\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\end{document}

